# -*- coding: utf-8 -*-
'''
Created on 20 жовт. 2015

@author: chmel

Tested with: Selenium WebDriver 2.48 on Linux platform. As a driver 'chromedriver' has been used. For first etst Turq has been used. It has been set with the expression:

path('/api/products').status(200).json([{
'productId' : '1',
'productName' : 'test1'
},
{
'productId' : '2',
'productName' : 'test2'
},
{
'productId' : '3',
'productName' : 'test3'
}])
'''
import unittest
import simplejson as json
import urllib2
import ConfigParser
from GitHub import *


class TestRest(unittest.TestCase):


    def setUp(self):
        conf = ConfigParser.ConfigParser()
        conf.read('etc/framework.ini')
        self.url = conf.get("REST", "host")
        self.turq_port=conf.get("REST", "port")
        driver = CustomWebDriver(conf.get("GIT", "webdriver"))
        self.webdriver = driver.get_driver()
        self.github_username = conf.get("GIT", "user")
        self.github_password = conf.get("GIT", "password")


    def tearDown(self):
        self.webdriver.close()


    def testRESTApi(self):
        #We should initialize api_products_req outside the "try" block to avoid initialization error in future :)
        api_products_req = urllib2.urlopen("http://%s:%s" %(self.url, self.turq_port))
        try:
            api_products_req = urllib2.urlopen("http://%s:%s/api/products" %(self.url, self.turq_port))
        except urllib2.HTTPError, he:
            self.fail("HTTP response code is not 200 OK as expected. Actual: %s" %he.code)            
        
        api_products_json = json.loads(api_products_req.read())
        
        for item in api_products_json:
            self.assertTrue("productId" in item.keys(), "No productId key is present")
            self.assertEqual("%s" %(api_products_json.index(item) + 1), item["productId"], "Incorrect product Id in record %s. Actual: %s. Expected: %s" %((api_products_json.index(item) + 1), item["productId"], (api_products_json.index(item) + 1)))
            self.assertEqual("test%s" %(api_products_json.index(item) + 1), item["productName"], "Incorrect product Name in record %s. Actual: %s. Expected: %s" %((api_products_json.index(item) + 1), item["productName"], ("test%s" %(api_products_json.index(item) + 1))))
    
    def testGITSite(self):
        initial_page = GitHubInitial(self.webdriver)
        self.assertTrue(initial_page.verify_if_element_exists(initial_page.navi_menu_item('Explore')), "Initial Page does not contain item 'Explore'")
        self.assertTrue(initial_page.verify_if_element_exists(initial_page.navi_menu_item('Features')), "Initial Page does not contain item 'Features'")
        user_workspace = initial_page.go_to_login_page().login(self.github_username, self.github_password)
        self.assertTrue(user_workspace.verify_if_element_exists(initial_page.navi_menu_item('Pull requests')), "Cannot find item 'Pull requests' in the user workspace")
        self.assertTrue(user_workspace.verify_if_element_exists(initial_page.navi_menu_item('Issues')), "Cannot find item 'Issues' in the user workspace")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()