# -*- coding: utf-8 -*-
'''
Created on 21 жовт. 2015

@author: chmel
'''

from GitHub import *

class InitialUserPage(GitHubPageTemplate):
    '''
    Represents initial user page after login
    '''


    def __init__(self, webdriver):
        GitHubPageTemplate.__init__(self, webdriver)
        