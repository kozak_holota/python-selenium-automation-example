# -*- coding: utf-8 -*-
'''
Created on 21 жовт. 2015

@author: chmel
'''
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from GitHub import *


class GitHubLoginPage(GitHubPageTemplate):
    '''
    Login Page for GitHub Page
    '''


    def __init__(self, webdriver):
        GitHubPageTemplate.__init__(self, webdriver)
    
    def login(self, username, password):
        login_field = self.webdriver.find_element_by_id('login_field')
        password_field = self.webdriver.find_element_by_id('password')
        submit_button = self.webdriver.find_element_by_name('commit')
        login_field.send_keys(username)
        password_field.send_keys(password)
        submit_button.click()
        try:
            WebDriverWait(self.webdriver, 30).until(EC.presence_of_element_located((By.ID, 'user-links')))
        except Exception, ex:
            raise ValueError("Cannot login with username '%s' and password '%s'. Error was: %s" %(username, password, ex))
        return InitialUserPage(self.webdriver)