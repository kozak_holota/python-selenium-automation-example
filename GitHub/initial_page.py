# -*- coding: utf-8 -*
'''
Created on 21 жовт. 2015

@author: Pavlo Mryhlotskyy
'''

import logging
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from GitHub import *

class GitHubInitial(GitHubPageTemplate):
    '''
    GitHub initial page
    '''


    def __init__(self, webdriver):
        GitHubPageTemplate.__init__(self, webdriver)
        self.webdriver.get('https://github.com')
        
    def go_to_login_page(self):
        self.webdriver.get('https://github.com/login')
        try:
            WebDriverWait(self.webdriver, 30).until(EC.presence_of_element_located((By.ID, 'login_field')))
        except Exception, ex:
            raise ValueError("Cannot navigate to Login Page. Error was: %s" %(ex))
        
        return GitHubLoginPage(self.webdriver)