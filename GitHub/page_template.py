# -*- coding: utf-8 -*-
'''
Created on 20 жовт. 2015

@author: chmel
'''

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import exceptions

class CustomWebDriver:
    '''
    Webdriver type factory
    '''


    def __init__(self, name):
        #self.drivers = {"firefox": webdriver.Firefox(), "chrome": webdriver.Chrome()}
        if name == "firefox":
            self.driver = webdriver.Firefox()
        elif name == "chrome":
            self.driver = webdriver.Chrome()
        else:
            raise ValueError("Unknown browser")
    
    def get_driver(self):
        return self.driver

class GitHubPageTemplate:
    '''
    Page Template for GitHub
    '''
    
    def __init__(self, webdriver):
        self.webdriver = webdriver
        self.navi_path = "//*[@class='header-nav left']/*[@class='header-nav-item']/*[contains(text(), '%s')]"
        
    def navi_menu_item(self, text):
        return self.navi_path %text
    
    def verify_if_element_exists(self, xpath):
        try:
            self.webdriver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        
        return True
    
    
            